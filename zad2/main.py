# stworzyc program ktory:
#     - przy pierwszym uruchomieniu tworzy plik oraz wpisuje do niego imie i nazwisko
#     - przy kazdym kolejnym uruchomieniu ma dopisywac do istniejacego pliku twoj wiek

# na koncu wypisz zawartosc pliku w konsoli


def fileExist(fileName):
    try:
        with open(fileName, "x") as fout:
            return False
    except FileExistsError:
            return True

# Your error handling goes here

def write_data_to_file(fileName):
    if fileExist("my_file.txt"):
        print("file exist")
        with open(fileName, "a") as my_file:
            print("18", file=my_file)
    else:
        print("file not exist")
        with open(fileName, "w") as my_file:
            print("Jacek Z", file=my_file)


def read_file_v0(fileName):
    with open(fileName, "r") as my_file:
        for line in my_file:
            print(line)


def read_file_v1(fileName):
    with open(fileName, "r") as my_file:
        for line in my_file:
            print(line, end="")


def read_filev2(fileName):
    with open(fileName, "r") as my_file:
        lines = my_file.readlines()

    print(lines)
    for line in lines:
        print(line, end="")


if __name__ == "__main__":
    fileName = "my_file.txt"
    write_data_to_file(fileName)
    read_filev2(fileName)
