import shelve
import pickle
def initPickle():
    with open("test_pickle", "wb") as test_file:
        pickle.dump("Jacek", test_file)
        pickle.dump("ziom", test_file)
        pickle.dump("Hej to ja", test_file)
        pickle.dump({"name": "Jacek", "age": 33, "opis": (1, "33", "casc")}, test_file)


def initShelve():
    with shelve.open("test") as test_file:
        test_file["imie"] = "Jacek"
        test_file["nazwisko"] = "ziom"
        test_file["opis"] = "Hej to ja"
        test_file["data"] = {"name": "Jacek", "age": 33, "opis": (1, "33", "casc")}

if __name__ == "__main__":
    # initShelve()
    with shelve.open("test") as test_file:
        data = test_file.get("nazwisko")
    print(data)