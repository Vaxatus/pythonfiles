from p_s import Osoba
from os import system, name
# data = [
#     ("Damian", "Wojtas", 33, "TO ja Damian", [3, 12, 28, 30]),
#     ("Ania", "Kotek", 55, "TO ja Kotek", [6, 1, 3, 16]),
#     ("Michal", "Ptaku", 12, "TO ja Ptaku", [9, 14, 22, 13]),
#     ("Kasia", "Jana", 39, "TO ja Kasia", [8, 12, 22, 19])
# ]
#
#
# def init():
#     osoby = []
#     for el in data:
#         fileName = el[0] + "_" + el[1] + ".txt"
#         person = Osoba(fileName) # inicjalizacja klasy Osoba (__init__)
#         person.set_data(el) # wczytanie danych z tablicy data
#         person.save() # zapis do pliku kazdej osoby osobno
#         osoby.append(person)
#
#     for el in data:
#         person = Osoba(el[0] + "_" + el[1] + ".txt")
#         print("Wczytalem osobe : " + person.imie)


def read_person_table():
    osoby = []
    with open("lista_osob.txt", "r") as persons_file:
        for i in persons_file:
            e = i.replace(" ", "_")
            person = Osoba(e[0:-1] + ".txt")
            osoby.append(person)
    return osoby


def getPersonsList():
    with open("lista_osob.txt", "r") as persons_file:
        return persons_file.readlines()


# in RUN >> EDIT CONFIGURATIONS >> enable Emulate terminal console
# for macs altho add ;TERM=xterm-color to enviroment varibles
def clear():
    # for windows
    if name == 'nt':
        _ = system('cls')
        # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


def choice1():
    clear()
    persons = getPersonsList()
    i = 1
    for person in persons:
        print("{0}. {1}".format(i, person.rstrip()))
        i += 1
    choice = input("q = EXIT: ")
    if choice == "q":
        return True
    osoby = read_person_table()
    num = int(choice)
    clear()
    if 0 < num <= osoby.__len__():
        person = osoby[num - 1]
        print(person.imie)

        print(person.imie)
        print(person.nazwisko)
        print(person.wiek)
        print(person.opis)
        print(person.tablicaObecnosci)
    input()
    return False


def choice2():
    imie = input("Imię : ")
    nazwisko = input("Nazwisko : ")
    wiek = int(input("Wiek : "))
    opis = input("Opis : ")
    # wczytanie tablicy obecnosci jako tablicy

    data = (imie, nazwisko, int(wiek), opis, [])
    with open("lista_osob.txt", "a") as persons_file:
        persons_file.write(data[0] + " " + data[1] + "\n")
    fileName = data[0] + "_" + data[1] + ".txt"
    print(fileName)
    person = Osoba(fileName)
    person.set_data(data)  # wczytanie danych z tablicy data
    person.save()  # zapis do pliku


def choice3():
    clear()


if __name__ == "__main__":
    #init()
    menu_name = ("1. Pokaz liste", "2. Dodaj osobe", "3. Sprawdz obecnosc", "q. Wyjscie")
    while True:
        clear()
        for item in menu_name:
            print(item)
        choice = input("Wybor : ")
        if choice == "q":
           break
        if choice == "1":
            if choice1():
                break
        elif choice == "2":
            if choice2():
                break
        elif choice == "3":
            if choice3():
                break

